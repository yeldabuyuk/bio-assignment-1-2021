{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bio-Informatics 2020-2021 - Assignment 1\n",
    "## Secondary structure prediction, influence of protein family and using evolutionary algorithm.\n",
    "### By Yelda Büyük || 0527366 || Master Computer Science || Vrije Universiteit Brussel\n",
    "This notebook contains the implementation of the GORIII algorithm and the discussion on the results. The code has been split up into modules to respect the seperation of concerns design principle.  *The following work is based on my own work of the previous year.*\n",
    "\n",
    "#### Structure of the notebook\n",
    "The notebook is structured as follows:\n",
    "* Implementations\n",
    "    * Data preprocessing \n",
    "    * GORIII \n",
    "    * Calculations of Q3 and MCC\n",
    "    * Protein family prediction\n",
    "* Running the algorithm\n",
    "* Evaluation\n",
    "\n",
    "## Implementations\n",
    "### ---- Data preprocessing\n",
    "The first part of the assignment is to preprocess the given datasets: dssp_info.txt and stride_info.txt. Both files contain the following information: protein code || protein chain code || protein sequence code || residue name || secondary structure. The code of the preprocessing module can be found in the file **Preprocessing.py**.\n",
    "\n",
    "In this part, given a protein, we split the data (with the **splitData()** method) into a *training amino acid squence, a training secondary sequence, the actual protein amino acid sequence* and *the actual protein secondary sequence*. We achieve this by applying the leave-one-out method. The given protein is excluded from the computed training sets. This results into the fact that each protein will have its own individual training sets.\n",
    "\n",
    "### ---- GOR III\n",
    "The goal of the GOR III algorithm is to generate a secondary structure prediction of a given protein with a theoretical accuracy of on average 63%. The code for the GORIII algorithm can be found in the module **GORIII.py**. The algorithm class consists out of the following structure:\n",
    "* Training phase\n",
    "    * Self information\n",
    "    * Pair information\n",
    "* Prediction phase\n",
    "\n",
    "#### Training phase\n",
    "Two types of information is obtained during the training phase: the self and the pair information of an amino acid. We leave the protein to be predicted out of the training phase by making use of the preprocessed dataset. As mentioned earlier, therefore, the training sets per protein will slightly differ. The training of the self information and the pair information is initiated by the **train()** method.\n",
    "\n",
    "##### Self information\n",
    "The first step is to calculate the self information. To do so, we follow the following formula:\n",
    "$$I(\\Delta S_{j};R_{j}) = log(f_{S_{j}, R_{j}}/f_{n-S_{j}, R_{j}}) + log(f_{n-S}/f_{S})$$\n",
    "\n",
    "The amino acids at position J with its current and the opposite states are counted first. This information will be stored into the self amino acid dictionary. Next, we count the used states in the actual protein secondary structure. These states will be compared with the overall states. The self information is calculated and trained with the **trainSelfInfo()** method within the GORIII class.\n",
    "\n",
    "##### Pair information\n",
    "The second step is to calculate the pair information (**countPairInfo()** method) of an amino acid.  We follow the following formula to achieve this:\n",
    "$$I(\\Delta S_{j};R_{j+m}|R_{j}) = log(f_{S_{j}, R_{j+m}, R_{j}}/f_{n-S_{j}, R_{j+m}, R_{j}}) + log(f_{n-S_{j}, R_{j}} / f_{S_{j}, R_{j}})$$\n",
    "\n",
    "To clarify what is done in this part; for every amino acid we look at a window of 16 wide. At the start of the sequence, this 16 wide window will be smaller because we start at position 0 but we look from the relative -8 to +8 positions. This indicates that for position 0, the window will be from position 0 to 8. As soon as there is a possible 16 wide window, the algorithm will take that window. This will start from position 8 and onwards. The opposite happens towards the end of the sequence.\n",
    "\n",
    "**The procedure:**\n",
    " * Per window we have the amino acid at position J.\n",
    " * Iterate over window. Combine amino acids position J and at position I to form a pair.\n",
    " * For every pair (excluding the pair of amino acid J-J), increment the count for that pair. \n",
    " * Repeat for entire amino acid sequence. \n",
    "\n",
    "#### Prediction phase\n",
    "The next step is to predict a secondary structure sequence for a given protein. For this part, we take a look at the following formula:\n",
    "$$ I(\\Delta S_{j};R_{1},...,R_{n}) \\approx I(\\Delta S_{j};R_{j}) + \\sum_{m=-8, m\\neq 0}^{m=8} I(\\Delta S_{j};R_{j+m}|R_{j})$$\n",
    "\n",
    "We use the information that we generated during the training phase to generate a prediction. We do this by predicting a state per amino acid in the sequence by making use of the mentioned formula. We combine the self and the pair information per window and select the state with the highest information per amino acid. The method **predictStatePerAminoAcid()** predicts a state per amino acid whereas the method **predict()** generates the prediction for the entire sequence.\n",
    "\n",
    "### ---- Calculations\n",
    "The following will discuss the Q3 and MCC measurements. The code can be found in the **Calculations.py** module. Both calculations can be initialized by calling the **run()** method on the results of the GORIII algorithm.\n",
    "#### Q3\n",
    "Q3 measures the accuracy of a predicted secondary structure sequence generated by the GORIII algorithm. A comparison between the predicted and the actual secondary structure sequence will be made when using this measurement. It gives insight to how many states have been correctly predicted. The method **calculateQ3()** measures this, we use the following formula in the module:\n",
    "$$ Q_{3} = N_{residues\\_correctly\\_predicted} / N_{residues\\_total} $$\n",
    "\n",
    "#### MCC\n",
    "Q3 measures the accuracy of the predicted states. MCC gives insight to how correct the accuracy is per each type of state. In the module the method **calculateMCC()** does this for us, following the following formula:\n",
    " $$ MCC = TP \\times TN - FP \\times FN / \\sqrt{(TP + FP)(TP + FN)(TN + FP)(TN + FN)} $$\n",
    " \n",
    "To break down the formula; \n",
    " * TP: true positives - both states are of the same type.\n",
    " * TN: true negatives - both states are not the given state.\n",
    " * FP: false positives - prediction state is the given state and actual residue are different.\n",
    " * FN: false negatives - the actual residue state is the given state and the predicted is different.\n",
    " \n",
    "### ---- Protein family prediction\n",
    "Next we would like to predict the protein family per protein sequence. We do this by following these steps (the code can be found in the module **ProteinFamily.py**):\n",
    " * Parse the CATH file.\n",
    " * Fetch all the secondary structure sequences of every protein except for the currently to-be-evaluated one.\n",
    " * Per sequence, count and compute % of the appearance of each state.\n",
    " * Repeat step 2 and 3 for the prediction sequences.\n",
    " * Save the state % for each helix, sheet and coil. \n",
    " * Create differences between each value and the value of the state of the prediction.\n",
    " * Choose the nearest % of the prediction %, it being the minimal value. This will be 1 for each state, thus obtaining 3 nearest protein families.\n",
    " * Apply majority rule; choose protein family with the maximal value.\n",
    " \n",
    "## Running the algorithm\n",
    "Now that we have discussed how we implemented the previous mentioned implementations for GORIII, MCC and Q3, we will take a look on how to combine everything to generate output files containing the results. We use the **Main.py** module to run the algorithm and calculate the accuracy measurements. To change the dataset, simply following the instructions in the comments in the cell below. In order to run the code, please uncomment the last 3 rows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from Main import *\n",
    "\n",
    "# Can change the datasets by replacing \"dssp_info.txt\" with \"stride_info.txt\" and \"dssp\" with \"stride\".\n",
    "##### RUN #####\n",
    "\n",
    "project = Main(\"dssp_info.txt\", \"dssp\")\n",
    "# project.run()\n",
    "# print(\"Finished running.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluation\n",
    "The last part, evaluation, will discuss the obtained results from both datasets. We will also discuss the Q3 and MCC values of these datasets, and evaluate the results obtained per protein family."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "ename": "IndexError",
     "evalue": "list index out of range",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mIndexError\u001b[0m                                Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-15-0d01af9f711a>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[0mproject\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mplotGraph\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;34m\"Results/stride_output.csv\"\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;34m\"Results/dssp_output.csv\"\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;36m4\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;34m\"STRIDE\"\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;34m\"DSSP\"\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;34m\"Figure 1: Q3 values of STRIDE vs DSSP\"\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;32m~\\Desktop\\Bio-Informatica-2021\\Main.py\u001b[0m in \u001b[0;36mplotGraph\u001b[1;34m(self, output1, output2, col, label1, label2, title)\u001b[0m\n\u001b[0;32m     54\u001b[0m     \u001b[1;31m# Function to compare a given value for two given datasets.\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     55\u001b[0m     \u001b[1;31m# Plot the graph.\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 56\u001b[1;33m     \u001b[1;32mdef\u001b[0m \u001b[0mplotGraph\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mself\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0moutput1\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0moutput2\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mcol\u001b[0m\u001b[1;33m,\u001b[0m\u001b[0mlabel1\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mlabel2\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mtitle\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m     57\u001b[0m         \u001b[0marr1\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mself\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mgetArrayCSV\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0moutput1\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mcol\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     58\u001b[0m         \u001b[0marr2\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mself\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mgetArrayCSV\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0moutput2\u001b[0m\u001b[1;33m,\u001b[0m\u001b[0mcol\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32m~\\Desktop\\Bio-Informatica-2021\\Main.py\u001b[0m in \u001b[0;36mgetArrayCSV\u001b[1;34m(self, csvfile, col)\u001b[0m\n\u001b[0;32m     48\u001b[0m             \u001b[0mnext\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mread\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     49\u001b[0m             \u001b[1;32mfor\u001b[0m \u001b[0mrow\u001b[0m \u001b[1;32min\u001b[0m \u001b[0mread\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 50\u001b[1;33m                 \u001b[0mprint\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mrow\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;36m3\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m     51\u001b[0m                 \u001b[0marr\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mappend\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mfloat\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mrow\u001b[0m\u001b[1;33m[\u001b[0m\u001b[0mcol\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     52\u001b[0m         \u001b[1;32mreturn\u001b[0m \u001b[0msorted\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0marr\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;31mIndexError\u001b[0m: list index out of range"
     ]
    }
   ],
   "source": [
    "project.plotGraph(\"Results/stride_output.csv\", \"Results/dssp_output.csv\", 3, \"STRIDE\", \"DSSP\", \"Figure 1: Q3 values of STRIDE vs DSSP\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
