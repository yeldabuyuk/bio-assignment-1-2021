import numpy as np 
# This module  contains the code for the calculations of Q3 and MCC of assignment 1 for the course Bio-Informatics 2021 by Yelda Büyük.

class Calculations:
    def __init__(self, predictedSecondaryStructureSeq, proteinSecondaryStructure):
        self.prediction = predictedSecondaryStructureSeq
        self.secondaryStructure = proteinSecondaryStructure
        self.states = ["H", "E", "C"]
        self.Q3 = 0
        self.MCC = 0 # Initial values
        

    # Calculte Q3 score. 
    # Uses the secondary predicted structure of a protein.
    # Make a comparison of the predicted and the actual secondary structure. 
    def calculateQ3(self):
        total_residues = 0
        correct_predicted_residues = 0
        for residue in range(len(self.prediction)):
            actual_ss_residue = self.secondaryStructure[residue]
            predicted_residue = self.prediction[residue]
            if actual_ss_residue == predicted_residue:
                correct_predicted_residues += 1
            total_residues += 1
        self.Q3 = correct_predicted_residues/total_residues

    # Calculate the MCC score.
    # Calculates the accuracy of the predicted states.
    def calculateMCC(self):
        mcc_counts = {}
        mcc_result = {}
        TP_idx = 0
        FP_idx = 1
        FN_idx = 2
        TN_idx = 3
        # For every state
        for state_idx in range(3):
            # Fetch state
            state = self.states[state_idx]
            if state not in mcc_counts:
                mcc_counts.update({state: [0,0,0,0]}) # TP, FP, FN, TN
            # Compare each predicted residue with the actual residue. Increment the correct count.
            for i in range((len(self.secondaryStructure))):
                actual_residue = self.secondaryStructure[i]
                predicted_residue = self.prediction[i]
                # Compare and increment.
                if (actual_residue == state) and (predicted_residue == state):
                    mcc_counts[state][TP_idx] += 1
                elif (actual_residue == state) and (predicted_residue != state):
                    mcc_counts[state][FN_idx] += 1
                elif (actual_residue != state) and (predicted_residue == state):
                    mcc_counts[state][FP_idx] += 1
                else:
                    mcc_counts[state][TN_idx] += 1
                # Use formula.
                #check for 0's and add 1
                if(mcc_counts[state][TP_idx] == 0) and (mcc_counts[state][FP_idx] == 0):
                    mcc_counts[state][TP_idx] += 1
                if(mcc_counts[state][TP_idx] == 0) and (mcc_counts[state][FN_idx] == 0):
                    mcc_counts[state][TP_idx] += 1
                if(mcc_counts[state][TN_idx] == 0) and (mcc_counts[state][FP_idx] == 0):
                    mcc_counts[state][TN_idx] += 1
                if(mcc_counts[state][TN_idx] == 0) and (mcc_counts[state][FN_idx] == 0):
                    mcc_counts[state][TN_idx] += 1
                
            num = ((mcc_counts[state][TP_idx] * mcc_counts[state][TN_idx]) - (mcc_counts[state][FP_idx] * mcc_counts[state][FN_idx]))
            denom = np.sqrt((mcc_counts[state][TP_idx] + mcc_counts[state][FP_idx]) * (mcc_counts[state][TP_idx] + mcc_counts[state][FN_idx]) * (mcc_counts[state][TN_idx] + mcc_counts[state][FP_idx]) * (mcc_counts[state][TN_idx] + mcc_counts[state][FN_idx]))
            result_per_state = num / denom
            if state not in mcc_result:
                mcc_result.update({state: 0}) # Helix, Sheet, Coil
            mcc_result[state] = result_per_state
        self.MCC = mcc_result
    
    # Calculate both scores.
    def run(self):
        self.calculateQ3()
        self.calculateMCC()