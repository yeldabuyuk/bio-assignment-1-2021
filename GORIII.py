# This module contains the code for the GOR III Algorithm of assignment 1 for the course Bio-Informatics 2021 by Yelda Büyük.
# Structure:
# Trainig Phase:
    # Self information
    # Pair information
# Prediction Phase
from Preprocessing import Preprocessing
from Calculations import * 

import numpy as np 

class GORIII:
    def __init__(self, protein, dataset):
        self.protein = protein
        self.preprocessing = Preprocessing(dataset)
        self.dic = self.preprocessing.residueDic
        self.codes = self.preprocessing.splitData(self.protein)
        self.trainingAcidSequence = self.codes[0]
        self.trainingSecondaryStructure = self.codes[1]
        self.proteinAcidSequence = self.codes[2]
        self.proteinSecondaryStructure = self.codes[3]
        self.states = ["H", "E", "C"]
        self.selfResultDic = {}
        self.selfAminoDic = {}
        self.selfSubsetDic = {}
        self.selfStatesDic = {}
        self.selfNotStatesDic = {}
        self.pairPairDic = {}
        self.pairNotPairDic = {}
        self.pairSubsetDic = {}
        self.pairAminoDic = {}
        self.rangeBegin = 0
        self.rangeEnd = 0
        self.stateIdx = 0
        self.notStatesArr = []
        self.prediction = ""

    #########################################
    ########## AUXILIARY FUNCTIONS ##########
    #########################################

    # Update and set the not states array. (the rest to the current state)
    def setOppositeStates(self, j):
        oppositeStates = ({"H": [1, 2], "E": [0, 2], "C": [0, 1]})
        self.notStatesArr =  oppositeStates[self.trainingSecondaryStructure[j]]

    # Given position i and j, return the relative position of i from j.
    def getRelativePos(self, i, j): 
        pairPos = i - j
        if pairPos < 0:
            return str(pairPos)
        else:
            return ('+' + str(pairPos))

    # Given a specific state,return the state index.
    def getStateIdx(self, state):
        states = ["H", "E", "C"]
        return states.index(state)

    # Given a specific key (can either be an amino acid or a state), increment the correct information in the corresponding dictionary. 
    def countType(self, key, dic):
        if key not in dic:
            dic.update({key: 1})
        else:
            dic[key] += 1
        return dic

    # Given a position J and the prediction information dictionary, add the predicted state for the amino acid at position J to the prediction string.
    def predictState(self, j, predictDic):
        aminoJ = self.proteinAcidSequence[j]
        arr = predictDic[aminoJ]
        maxValue = max(arr)
        if maxValue in arr:
            valueIdx = arr.index(maxValue)
        self.prediction += self.states[valueIdx] # Adds the predicted state to the prediction.

    # For a given key and an array of dictionaries, initialize the value of the key within the dictionary.
    # Returns the dictionary of arrays for further use.
    def initializeDic(self, key, arrOfDic):
        for i in range(len(arrOfDic)):
            if key not in arrOfDic[i]:
                arrOfDic[i].update({key: [0,0,0]})
        return arrOfDic

    # Form a three dimensional key with the aminoJ, aminoI and the relative position of the pair.
    # Boolean = True -; use the trainingAcidSequence
    #           False -; use the proteinAcidSequence
    def getPairKey(self, boolean, i, j):
        if boolean:
            aminoJ = self.trainingAcidSequence[j]
            aminoI = self.trainingAcidSequence[i]
        else:
            aminoJ = self.proteinAcidSequence[j]
            aminoI = self.proteinAcidSequence[i]
        relativePos = self.getRelativePos(i,j)
        return (aminoJ + relativePos + aminoI)
    
    # Given an amino acid and state index, increment the count.
    def incrementDic(self, amino, state, dic): 
        if amino not in dic:
            dic.update({amino: [[0,0], [0,0], [0,0]]}) 
        state_idx = self.getStateIdx(state)
        dic[amino][state_idx][0] += 1
        return dic

    # Count specific state of amino into dictionary. Update the corresponding amino dictionary.
    # Boolean = True ;- use the self amino dictionary.
    #           False ;- use the pair amino dictionary.
    def countPerAmino(self, pos, boolean):
        amino = self.trainingAcidSequence[pos]
        state = self.trainingSecondaryStructure[pos]
        if boolean:
            dic = self.selfAminoDic
            self.selfAminoDic = self.incrementDic(amino, state, dic)
        else:
            dic = self.pairAminoDic
            self.pairAminoDic = self.incrementDic(amino, state, dic)

    # Per key, add the "not" states.
    # Total amount amino acids per type form the subset of an amino acid.
    def updateNotStates(self, dic, subsetDic,):
        positiveCol = 1
        negativeCol = 0
        for key in dic:
            value = dic[key]
            totalVal = subsetDic[key]
            for i in range(3): 
                if totalVal != 1:
                    if totalVal != dic[key][i][negativeCol]:
                        dic[key][i][positiveCol] = totalVal - value[i][negativeCol] 
                    else:
                        dic[key][i][positiveCol] = (totalVal + 1) - value[i][negativeCol]
        return dic
    
    # Add the not states to the corresponding amino acid dictionary.
    # Boolean = True ;- use the self amino dictionary and the self subset dictionary.
    #           False ;- use the pair amino dictionary and the pair subset dictionary.
    def addNotStates(self, boolean):
        if boolean:
            dic = self.selfAminoDic
            subsetDic = self.selfSubsetDic
            self.selfAminoDic = self.updateNotStates(dic, subsetDic)
        else:
            dic = self.pairAminoDic
            subsetDic = self.pairSubsetDic
            self.pairAminoDic = self.updateNotStates(dic, subsetDic)

    # Calculate information given two denominators and numerators.
    def calculateInfo(self, denom1, denom2, num1, num2):
        if (denom1 != 0) and (denom2 != 0):
            return np.log(num1/denom1) + np.log(num2/denom2)
        elif (denom1 != 0) and (denom2 == 0):
            return np.log(num1/denom1)
        elif (denom1 == 0) and (denom2 != 0):
            return np.log(num2/denom2)
        else:
            return 0

    ######################################
    ########## SELF INFORMATION ##########
    ######################################

    #  Calculte the self information per amino acid per state.
    def calculateSelfInfo(self):
        for key in self.selfAminoDic:
            value = self.selfAminoDic[key]
            # We use range 3 because we have 3 states.
            for i in range(3):
                fSR = value[i][0]
                fNSR = value[i][1]
                state = self.states[i]
                fNS = self.selfNotStatesDic[state]
                fS = self.selfStatesDic[state]
                if key not in self.selfResultDic:
                    self.selfResultDic.update({key: [0,0,0]})            
                self.selfResultDic[key][i] = self.calculateInfo(fNSR, fS, fSR, fNS)

    # Train self information. 
    # Calculate the self information during the training phase. 
    def trainSelfInfo(self):
        for j in range(len(self.trainingAcidSequence)):
            aminoJ = self.trainingAcidSequence[j]
            state = self.trainingSecondaryStructure[j]
            # Count amino acids at position j with its state.
            self.countPerAmino(j, True)
            # Count subsets per amino
            self.selfSubsetDic = self.countType(aminoJ, self.selfSubsetDic)
            # Count states
            self.selfStatesDic = self.countType(state, self.selfStatesDic)
        # Add non states of amino_j with a state.
        self.addNotStates(True)
        # Add non states.
        totalStates = 0
        for state in self.selfStatesDic:
            totalStates += self.selfStatesDic[state]
        for state in self.selfStatesDic:
            if state not in self.selfNotStatesDic:
                self.selfNotStatesDic.update({state: 0})
            self.selfNotStatesDic[state] = totalStates - self.selfStatesDic[state]
            
        # Calculate self info.
        self.calculateSelfInfo()

    ######################################
    ########## PAIR INFORMATION ##########
    ######################################

    # Count the pairs per window within a range.
    # Generates pair dictionary with the corresponding counts and opposites information.
    # Three states are known per pair. Each state contains the same key for the same pair. 
    def countPairInfoWindow(self, j):
        for i in range(self.rangeBegin, self.rangeEnd):
            # Can not have the pair where the two amino acids are equal.
            if j != i:
            # Form a key.
                pairKey = self.getPairKey(True, i, j)
                arrDics = self.initializeDic(pairKey, [self.pairPairDic, self.pairNotPairDic])
                self.pairPairDic = arrDics[0]
                self.pairNotPairDic = arrDics[1]
                # Increment count.
                self.pairPairDic[pairKey][self.stateIdx] += 1
                # Increment the not states.
                for notStateIdx in self.notStatesArr:
                    self.pairNotPairDic[pairKey][notStateIdx] += 1

    # Iterate over the training amino acid sequence. We increment the correct keys.
    # Per amino acid the following happens:
        # Look at its window, count the pairs per window.
    # Sequence is split into four parts:
        # First and second part form the beginning part.
        # Third part forms the middle part.
        # Last part forms the ending of the sequence.
    # When pair counting is finished, the amino acid at position j with its state is counted.
    def countPairInfo(self):
        for j in range(len(self.trainingAcidSequence)):
            # Fetch state index.
            self.stateIdx = self.getStateIdx(self.trainingSecondaryStructure[j])
            # Set the not states array.
            self.setOppositeStates(j)
            # Calculate the information per window. Add result to the result dictionary.
            # When we come across the same amino acid, we repeat this process. If a specific state has higher value
            # than the one noted in the dictionary, we replace it.
            # Window from 0 to 7.
            if j == 0:
                self.rangeBegin = 0
                self.rangeEnd = 8
                self.countPairInfoWindow(j)
            elif j <= 8:
                self.rangeBegin = 0
                self.rangeEnd = j + 8
                self.countPairInfoWindow(j)
            elif (j > 8) and (j < ((len(self.trainingAcidSequence) - 8))):  # For the pairs between this interval.
                self.rangeBegin = j - 8
                self.rangeEnd = j + 8
                self.countPairInfoWindow(j)
            else: # last part
                self.rangeBegin = j - 8
                self.rangeEnd = len(self.trainingAcidSequence)
                self.countPairInfoWindow(j)
        ## Now calculate subset information.
        # Iterate over sequence.
        # For every amino acid, keep track how many times they are present -- generating subsets per amino acid.
        for i in range((len(self.trainingAcidSequence))):
            # Count the subsets per amino acid.
            self.pairSubsetDic = self.countType(self.trainingAcidSequence[i],  self.pairSubsetDic)
            # For every amino acid we encounter, check its state and increment the corresponding position in the 
            # dictionary.
            self.countPerAmino(i, False)
            # Add non states per amino.
            self.addNotStates(False) # not is column two

    #####################################
    ########## COMINE TRAINING ##########
    #####################################
    def train(self):
        self.trainSelfInfo()
        self.countPairInfo()

    ################################
    ########## PREDICTION ##########
    ################################

    # Calculate result per pair.
    # If the key is not in the pair dictionary, we do not have information from the training set for this
    # pair of amino acids at a relative position and are forced to use the dummy data of an array of 0's.
    def calculateResultPerPair(self, i, j, pairSumDic):
        # In the event that the dictionary contains the value for the given key.
        key = self.getPairKey(False, i, j)
        aminoJ = key[:1]
        if key in self.pairPairDic:
            pairValue = self.pairPairDic[key]
            notPairValue = self.pairNotPairDic[key]
            aminoValue = self.pairAminoDic[aminoJ]
            # Per state
            for i in range(3):
                # Pair's
                fSRR = pairValue[i]
                fNSRR = notPairValue[i]
                # Amino's
                fSR = aminoValue[i][0]
                fNSR = aminoValue[i][1]
                # Calculate the information.
                if aminoJ not in pairSumDic:
                    pairSumDic.update({aminoJ: [0,0,0]})
                # Calculate and add the information to the dictionary.
                pairSumDic[aminoJ][i] += self.calculateInfo(fNSRR, fSR, fSRR, fNSR)
        else:
            if aminoJ not in pairSumDic:
                pairSumDic.update({aminoJ: [0,0,0]})
        return pairSumDic

    # Predict a state per amino acid.
    def predictStatePerAminoAcid(self, j, pairSumDic, predictDic):
        aminoJ = self.proteinAcidSequence[j]
        for i in range(self.rangeBegin, self.rangeEnd):
            if i != j:
                # Calculate information per pair for every state.
                # Dictionary to keep track of results.
                sumPairDic = self.calculateResultPerPair(i, j, pairSumDic)
        # For every state, calculate the prediction information for amino acid at position j.
        for i in range(3):
            info = pairSumDic[aminoJ][i] + self.selfResultDic[aminoJ][i]
            predictDic[aminoJ][i] += info
        # Finally, predict the state for the amino acid at position j.
        self.predictState(j, predictDic)
    
    # Generate the prediction for a protein.
    # Steps:
        # Iterate over the entire amino acid sequence.
        # Per amino acid at position j:
            # Look at window from j - 8 to j + 8.
            # Calcualte self information for each three states.
            # Choose state with highest self information as prediction.
            # Append predicted state to the final prediction sequence.
    def predict(self):
        self.prediction = ''
        for j in range((len(self.proteinAcidSequence))):
            aminoJ = self.proteinAcidSequence[j]
            pairSumDic = {}
            predictDic = {}
            arrayDics = self.initializeDic(aminoJ, [predictDic, self.selfResultDic])
            predictDic = arrayDics[0]
            self.selfResultDic = arrayDics[1]
            if j == 0:
                self.rangeBegin = 0
                self.rangeEnd = 8
                self.predictStatePerAminoAcid(j, pairSumDic, predictDic)
            elif j <= 8:
                self.rangeBegin = 0
                self.rangeEnd = j + 8
                self.predictStatePerAminoAcid(j, pairSumDic, predictDic)
            elif (j > 8) and (j < ((len(self.proteinAcidSequence) - 8))):
                self.rangeBegin = j - 8
                self.rangeEnd = j + 8
                self.predictStatePerAminoAcid(j, pairSumDic, predictDic)
            else: # length j >= length of protein_seq - 8
                self.rangeBegin = j - 8
                self.rangeEnd = len(self.proteinAcidSequence)
                self.predictStatePerAminoAcid(j, pairSumDic, predictDic)

    #####################################################
    ########## COMBINE TRAINING AND PREDICTION ##########
    #####################################################
    def run(self):
        self.train()
        self.predict()

