# Main file to run the project from.
from Calculations import Calculations
from Preprocessing import Preprocessing
from GORIII import GORIII
from ProteinFamily import ProteinFamily
import csv
import matplotlib.pyplot as plt
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
import numpy as np

class Main:
    def __init__(self, datasetName, outputFile):
        self.datasetName = datasetName
        self.dataset = Preprocessing(datasetName).dataset
        self.outputFile = outputFile
        self.lst = {}
        self.gorCount = 0
        self.alphaData = 0
        self.betaData = 0
        self.alphaBetaData = 0
        self.noneData = 0
    
    def run(self):
        for row in self.dataset:
            if row[0] not in self.lst:
                self.lst.update({row[0]: 0})
        with open((self.outputFile + "_output.csv"),mode='w') as csvFile:
            writer = csv.writer(csvFile)
            writer.writerow(["filename", "protein", "prediction", "Q3", "MCC_Helix", "MCC_Sheet", "MCC_Coil", "Class prediction"])
            for protein in self.lst:
                gor3 =  GORIII(str(protein), self.datasetName)
                gor3.run()
                calculations = Calculations(gor3.prediction, gor3.proteinSecondaryStructure)
                calculations.run()
                q3Result = calculations.Q3
                self.gorCount += 1
                self.lst[protein] = q3Result
                mccResult = calculations.MCC
                predictClass = ProteinFamily(gor3.proteinSecondaryStructure, gor3.prediction, self.datasetName)
                predictClass.run()
                predictClass.predict()
                predictedClassFamily = predictClass.predictedProteinFamily
                #write result
                writer.writerow([self.datasetName, protein, gor3.prediction, q3Result, mccResult["H"], mccResult["E"], mccResult["C"], predictedClassFamily])

    

    # Function to generate an array with information to be used to plot data.
    def getArrayCSV(self, csvfile, col):
        arr = []
        with open(csvfile) as csv_file:
            read = csv.reader(csv_file, delimiter = ';')
            next(read)
            for row in read:
                arr.append(float(row[col]))
        return sorted(arr)

    # Function to compare a given value for two given datasets.
    # Plot the graph.
    def plotGraph(self, output1, output2, col,label1, label2, title):
        arr1 = self.getArrayCSV(output1, col)
        arr2 = self.getArrayCSV(output2,col)
        fig, ax = plt.subplots(figsize=(10,8))
        ax.plot(arr1, label=label1)
        ax.plot(arr2, label=label2)
        ax.set_title(title) # set a title
        ax.legend() # show a legend on the plot
        plt.show()

    def generateAveragePerFamily(self, col, label):
        self.alphaData = round(np.mean(self.getArrayCSV('Results/alpha_family_results.csv', col)), 2)
        self.betaData = round(np.mean(self.getArrayCSV('results/beta_family_results.csv', col)), 2)
        self.alphaBetaData = round(np.mean(self.getArrayCSV('results/alpha_beta_family_results.csv', col)), 2)
        self.noneData = round(np.mean(self.getArrayCSV('results/none_family_results.csv', col)),2)

        #Display results
        print("The overall " + label + " of the Alpha protein family is: " + str((self.alphaData * 100)) + " %.")
        print("The overall " + label + " of the Alpha/Beta protein family is: " + str((self.alphaBetaData * 100)) + " %.")
        print("The overall " + label + " of the Beta protein family is: " + str((self.betaData * 100)) + " %.")
        print("The overall " + label + " of the None protein family is: " + str((self.noneData * 100)) + " %.")    
##Run
# Can change the datasets by replacing "dssp_info.txt" with "stride_info.txt" and "dssp" with "stride".
#project = Main("dssp_info.txt", "dssp")
#project.run()
#print("Finished running.")