import numpy as np 
# This module contains the code for the preprocessing phase of assignment 1 for the course Bio-Informatics 2021 by Yelda Büyük.
class Preprocessing:
    def __init__(self, dataset):
        self.dataset = np.loadtxt("Data/" + str(dataset), dtype=np.str, delimiter='\t')
        self.residueDic = {'ALA': 'A', 'ARG': 'R', 'ASN': 'N', 'ASP': 'D', 'CYS': 'C', 'PHE': 'F', 'GLN': 'Q', 'GLU': 'E', 'GLY': 'G', 'HIS': 'H', 'ILE': 'I', 'LEU': 'L', 'LYS': 'K', 'MET': 'M', 'PRO': 'P', 'SER': 'S', 'THR': 'T', 'TRP': 'W', 'TYR': 'Y', 'VAL': 'V'}
        self.secondaryStructureDic = {'Helix': 'H', 'Beta': 'E', 'Coil': 'C', 'Other': 'C'}

    # Map the three letter residue codes to one letter codes.
    def mapResidueCode(self, threeLetterCode):
        if threeLetterCode not in self.residueDic:
            return 'X'
        else:
         return self.residueDic[threeLetterCode]

    #Map the three letter secondary structure codes to one letter codes.
    def mapSecondaryStructureCode(self, threeLetterCode):
        if threeLetterCode not in self.secondaryStructureDic:
            return 'X'
        else:
         return self.secondaryStructureDic[threeLetterCode]

    # Given a protein, we want to leave it out from the entire sequence following the "leave-one-out" method.
    # The protein itself is accumulated and its state information. When we do not the current protein, we use the data as the training data for this specific protein.
    # Each protein will thus generate their individual training data.
    def splitData(self, protein):
        training_acid_seq = ''
        training_secondary_seq = ''
        protein_acid_seq = ''
        protein_secondary_seq = ''
        # Fetching the individual data from the columns.
        for row in self.dataset:
            protein_code = row[0]
            amino_code = row[3]
            state_code = row[4]
            # Leaving one out. Protein to be predicted is left out of the training set.
            if protein_code != protein:
                # Map the codes and add it to the correct string.
                training_acid_seq += self.mapResidueCode(amino_code)
                training_secondary_seq += self.mapSecondaryStructureCode(state_code)
            else:
                protein_acid_seq += self.mapResidueCode(amino_code)
                protein_secondary_seq += self.mapSecondaryStructureCode(state_code)
        return [training_acid_seq, training_secondary_seq, protein_acid_seq, protein_secondary_seq]