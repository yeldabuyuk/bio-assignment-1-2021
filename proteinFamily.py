import numpy as np 
from Preprocessing import Preprocessing

class ProteinFamily:
    def __init__(self, protein, prediction, dataset):
        self.protein = protein
        self.prediction = prediction
        self.dataset = Preprocessing(dataset).dataset
        self.trainingSet = np.loadtxt('Data/cath_info.txt', dtype= np.str, delimiter = "\t")
        self.actualClass = {} 
        self.allSecondStructs = {}
        self.percentages = [[],[],[]]
        self.proteinNames = []
        self.secondaryStructureDic = {'Helix': 'H', 'Beta': 'E', 'Coil': 'C', 'Other': 'C'}
        self.predictionPercentages = {}
        self.predictedProtein = {}
        self.states = ["H", "E", "C"]
        self.predictedProteinFamily = ""
        self.countDic = {}


    # Map the three letter residue codes to one letter codes.
    def mapResidueCode(self, threeLetterCode):
        if threeLetterCode not in self.secondaryStructureDic:
            return 'X'
        else:
         return self.secondaryStructureDic[threeLetterCode]

    # Given a specific state,return the state index.
    def getStateIdx(self, state):
        return self.states.index(state)

    # Parse the CATH file and initialize the actualClass dictionary.
    def parseTrainingSet(self):
        for row in self.trainingSet:
            proteinName = row[0]
            proteinClass = row[2]
            # Leave self out.
            if proteinName != self.protein:
                self.actualClass.update({proteinName: proteinClass})
    
    # Parse the secondary structure sequences of all proteins except the current evaluated one.
    def parseSecondaryStructure(self):
        for row in self.dataset:
            proteinName = row[0]
            secondaryStructure = self.mapResidueCode(row[4])
            if proteinName not in self.allSecondStructs:
                self.allSecondStructs.update({proteinName: ''})
            #append
            self.allSecondStructs[proteinName] += secondaryStructure

     # Calculate % of H, E and C in a secondary structure sequence.
    def calculateStatePercentages(self):
        for row in self.allSecondStructs:
            self.proteinNames.append(row)
            self.countDic = {}
            second = self.allSecondStructs[row]
            for i in range(len(second)):
                if second[i] not in self.countDic:
                    self.countDic.update({second[i] : 0})
                self.countDic[second[i]] += 1
            for struct in self.countDic:
                self.countDic[struct] = self.countDic[struct] / len(second)
            for struct in self.countDic:
                index = self.getStateIdx(struct)
                self.percentages[index].append(self.countDic[struct])
            
     # Calculate % of H, E, C in own prediction
    def calculatePredictionPercentages(self):
        for struct in self.prediction:
            if struct not in self.countDic:
                self.countDic.update({struct: 0})
            self.countDic[struct] += 1
        for struct in self.countDic:
            self.countDic[struct] = self.countDic[struct]/len(self.prediction)
        self.predictionPercentages = self.countDic

    # Map each state value to a new dictionary, map them to new arrays, select minimum value and 
    # fetch the three closests protein families. Apply the majority rule to predict a protein family.s
    def majorityRule(self):
        for i in range(len(self.percentages)):
            currentArr = self.percentages[i]
            state = self.states[i]
            if state in self.predictionPercentages:
                value = self.predictionPercentages[state]
                absArr = list(map(lambda x : abs(value - x) if x > 0 else (value + abs(x)), currentArr))
                nearest = np.amin(absArr)
                proteinN = self.proteinNames[absArr.index(nearest)]
                if proteinN in self.actualClass:
                    classN = self.actualClass[proteinN]
                else:
                    classN = "None"
                if classN not in self.predictedProtein:
                    self.predictedProtein.update({classN: 0})
                self.predictedProtein[classN] += 1
    
    # Combine everything.
    def run(self):
        self.parseTrainingSet()
        self.parseSecondaryStructure()
        self.calculateStatePercentages()
        self.calculatePredictionPercentages()
        self.majorityRule()

    # Generate a protein family class prediction.
    def predict(self):
        self.predictedProteinFamily = max(self.predictedProtein)

